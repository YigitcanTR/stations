package com.yigitcan.stations;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
// Created by Yiğitcan ÖZTÜRK - yigit.can5403@gmail.com
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient; // to get current location
    private Location mLastKnownLocation; // to get last location
    private LocationCallback locationCallback;
    private Marker currentLocationMarker;
    private final int DEFAULT_ZOOM = 9;
    private boolean click_state = true; // to prevent click bug
    Button get_location; // location button
    ZoomControls zoomControls; // zoom buttons
    ImageButton cancel; //cancel button for cancelling the route
    HashMap<Marker, String> hashMap = new HashMap<Marker, String>(); // to get data from the marker
    List<LatLng> lngList = new ArrayList<>(); // to get location lists from api
    String api_url = "https://demo.voltlines.com/case-study/5/stations/"; // our api url

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        cancel = (ImageButton) findViewById(R.id.cancel_route);
        zoomControls = (ZoomControls) findViewById(R.id.zoom);
        get_location = (Button) findViewById(R.id.my_location);
        showLocationPermission();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MapsActivity.this);

        get_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mLastKnownLocation.reset();
                showLocationPermission();
                getDeviceLocation();
            }
        });

        zoomControls.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
                //String.valueOf(mMap.getCameraPosition().zoom)
            }
        });

        zoomControls.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
                //String.valueOf(mMap.getCameraPosition().zoom)
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMap.clear();
                getDeviceLocation();
                get_location.setVisibility(View.VISIBLE);
                LatLng voltline = new LatLng(41.113813, 29.021563); // Volt Lines Location
                mMap.addMarker(new MarkerOptions()
                        .position(voltline)
                        .title("Volt Lines")
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.noun_work_953270)));
                fetchDataforStations();
                Toast.makeText(MapsActivity.this, "You cancelled the route.", Toast.LENGTH_SHORT).show();
                cancel.setVisibility(View.INVISIBLE);
            }
        });

        fetchDataforStations();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng voltline = new LatLng(41.113813, 29.021563);  // Volt Lines Location
        mMap.addMarker(new MarkerOptions()
                .position(voltline)
                .title("Volt Lines")
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.noun_work_953270)));

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                try {
                    String data = hashMap.get(marker);
                    String[] parts = data.split(","); // get data from market with split method
                    String id = parts[0]; // before "," get
                    if (id != null) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showQuestionDialog(marker);
                            }
                        }, 1000);
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
                return false;
            }
        });

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(2500);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);

        SettingsClient settingsClient = LocationServices.getSettingsClient(MapsActivity.this);
        Task<LocationSettingsResponse> task = settingsClient.checkLocationSettings(builder.build());


        task.addOnSuccessListener(MapsActivity.this, new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                getDeviceLocation();
            }
        });

        task.addOnFailureListener(MapsActivity.this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    try {
                        resolvable.startResolutionForResult(MapsActivity.this, 51);
                    } catch (IntentSender.SendIntentException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 51) {
            if (resultCode == RESULT_OK) {
                getDeviceLocation();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(MapsActivity.this);
        fetchDataforStations();
    }

    // Draw polyline on map
    public void drawPolyLineOnMap(List<LatLng> list) {
        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(R.color.line_color));
        polyOptions.width(15);
        polyOptions.addAll(list);

        mMap.clear();
        mMap.addPolyline(polyOptions);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng latLng : list) {
            builder.include(latLng);
        }

        final LatLngBounds bounds = builder.build();

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 100);
        mMap.animateCamera(cu);

        mMap.addMarker(new MarkerOptions()
                .position(list.get(list.size() - 1))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.oval_copy)));
        mMap.addMarker(new MarkerOptions()
                .position(list.get(0))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.oval_copy_2)));
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showVerifyDialog();
            }
        }, 1000);
    }

    private void fetchDataforBooking(String id) {
        RequestQueue queue = Volley.newRequestQueue(MapsActivity.this);
        final String url = api_url + id;
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                            try {
                                JSONObject object = new JSONObject(response); // extraction of the response because response is json
                                JSONArray array = object.getJSONArray("polyline"); // extraction of the array called articles
                                for (int i = 0; i <= array.length(); i++) { // pull all the array news until last of the data

                                    String[] parts = array.getString(i).split(",");
                                    String v = parts[0]; // before "," get
                                    String v1 = parts[1]; // after "," get

                                    lngList.add(new LatLng(Float.parseFloat(v), Float.parseFloat(v1)));
                                    if (i<array.length()) {
                                        Handler handler = new Handler();
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    drawPolyLineOnMap(lngList);
                                                } catch (Exception e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }, 1000);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                showErrorDialog(id);
                            }
                        }, 500);
                    }
                }
        );
        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    private void showErrorDialog(String id) {
        final Dialog dialog = new Dialog(MapsActivity.this);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.custom_dialog_question);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LinearLayout circle_lyt = (LinearLayout) dialog.findViewById(R.id.circle_layout);
        circle_lyt.setBackground(ContextCompat.getDrawable(MapsActivity.this, R.drawable.oval_draw2));
        LinearLayout title_lyt = (LinearLayout) dialog.findViewById(R.id.title_layout);
        title_lyt.setBackground(ContextCompat.getDrawable(MapsActivity.this, R.drawable.rectangle_draw_3));
        ImageView imageView = (ImageView) dialog.findViewById(R.id.icon_view);
        imageView.setImageDrawable(ContextCompat.getDrawable(MapsActivity.this, R.drawable.exclamation));
        TextView txt_title = (TextView) dialog.findViewById(R.id.title_name);
        TextView txt_count = (TextView) dialog.findViewById(R.id.station_number);
        TextView txt_dialog = (TextView) dialog.findViewById(R.id.text_dialog);
        CardView btn_cancel = (CardView) dialog.findViewById(R.id.no);
        CardView btn_try = (CardView) dialog.findViewById(R.id.yes);
        TextView txt_yes = (TextView) dialog.findViewById(R.id.yes_txt);
        TextView txt_no = (TextView) dialog.findViewById(R.id.no_txt);
        txt_title.setText("Error!");
        txt_count.setVisibility(View.INVISIBLE);
        txt_dialog.setText(getResources().getString(R.string.info_text));
        txt_yes.setText("Try Again");
        txt_no.setText("Cancel");
        btn_try.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                click_state = true;
                dialog.dismiss();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        fetchDataforBooking(id);
                    }
                }, 1000);
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                get_location.setVisibility(View.VISIBLE);
                click_state = true;
                dialog.dismiss();
            }
        });
        dialog.setCancelable(false);
        if (click_state) {
            dialog.show();
            click_state = false;
        }
    }

    private void showQuestionDialog(Marker marker) {
        final Dialog dialog = new Dialog(MapsActivity.this);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.custom_dialog_question);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LinearLayout circle_lyt = (LinearLayout) dialog.findViewById(R.id.circle_layout);
        circle_lyt.setBackground(ContextCompat.getDrawable(MapsActivity.this, R.drawable.oval_draw));
        LinearLayout title_lyt = (LinearLayout) dialog.findViewById(R.id.title_layout);
        title_lyt.setBackground(ContextCompat.getDrawable(MapsActivity.this, R.drawable.rectangle_draw_2));
        ImageView imageView = (ImageView) dialog.findViewById(R.id.icon_view);
        imageView.setImageDrawable(ContextCompat.getDrawable(MapsActivity.this, R.drawable.question));
        TextView txt_dialog = (TextView) dialog.findViewById(R.id.text_dialog);
        CardView btn_no = (CardView) dialog.findViewById(R.id.no);
        CardView btn_yes = (CardView) dialog.findViewById(R.id.yes);
        TextView txt_title = (TextView) dialog.findViewById(R.id.title_name);
        TextView txt_count = (TextView) dialog.findViewById(R.id.station_number);
        TextView txt_yes = (TextView) dialog.findViewById(R.id.yes_txt);
        TextView txt_no = (TextView) dialog.findViewById(R.id.no_txt);
        String data = hashMap.get(marker);
        String[] parts = data.split(",");
        String id = parts[0]; // before "," get
        String count = parts[1]; // after "," get
        txt_dialog.setText(getResources().getString(R.string.question_text));
        txt_title.setText(marker.getTitle());
        txt_count.setText(count);
        txt_yes.setText("Yes");
        txt_no.setText("No, thanks");

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lngList.clear();
                fetchDataforBooking(id);
                click_state = true;
                get_location.setVisibility(View.INVISIBLE);
                dialog.dismiss();
            }
        });

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                click_state = true;
            }
        });
        dialog.setCancelable(false);
        if (click_state) {
            dialog.show();
            click_state = false;
        }
    }

    private void showVerifyDialog() {
        final Dialog dialog = new Dialog(MapsActivity.this);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.custom_dialog_question);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        LinearLayout circle_lyt = (LinearLayout) dialog.findViewById(R.id.circle_layout);
        circle_lyt.setBackground(ContextCompat.getDrawable(MapsActivity.this, R.drawable.oval_draw3));
        LinearLayout title_lyt = (LinearLayout) dialog.findViewById(R.id.title_layout);
        title_lyt.setBackground(ContextCompat.getDrawable(MapsActivity.this, R.drawable.rectangle_draw_4));
        ImageView imageView = (ImageView) dialog.findViewById(R.id.icon_view);
        imageView.setImageDrawable(ContextCompat.getDrawable(MapsActivity.this, R.drawable.bitmap));
        TextView txt_dialog = (TextView) dialog.findViewById(R.id.text_dialog);
        CardView btn_ok = (CardView) dialog.findViewById(R.id.yes);
        CardView btn_cancel = (CardView) dialog.findViewById(R.id.no);
        TextView txt_title = (TextView) dialog.findViewById(R.id.title_name);
        TextView txt_count = (TextView) dialog.findViewById(R.id.station_number);
        TextView txt_ok = (TextView) dialog.findViewById(R.id.yes_txt);
        txt_title.setText("Your reservation has completed!");
        txt_dialog.setText(getResources().getString(R.string.verify_text));
        txt_ok.setText("OK");
        txt_count.setVisibility(View.INVISIBLE);
        btn_cancel.setVisibility(View.GONE);
        cancel.setVisibility(View.VISIBLE);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                click_state = true;
            }
        });
        dialog.setCancelable(false);
        if (click_state) {
            dialog.show();
            click_state = false;
        }
    }

    @SuppressLint("MissingPermission")
    private void getDeviceLocation() {
        mFusedLocationProviderClient.getLastLocation()
                .addOnCompleteListener(new OnCompleteListener<Location>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() ) {
                            if (task.isSuccessful()) {
                                if (null != currentLocationMarker) {
                                    currentLocationMarker.remove();
                                }
                                mLastKnownLocation = task.getResult();
                                if (mLastKnownLocation != null) {
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                                    currentLocationMarker = mMap.addMarker(new MarkerOptions()
                                            .position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()))
                                            .title("You are here")
                                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.oval)));
                                } else {
                                    final LocationRequest locationRequest = LocationRequest.create();
                                    locationRequest.setInterval(5000);
                                    locationRequest.setFastestInterval(2500);
                                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                                    locationCallback = new LocationCallback() {
                                        @Override
                                        public void onLocationResult(LocationResult locationResult) {
                                            super.onLocationResult(locationResult);
                                            if (locationResult == null) {
                                                return;
                                            }
                                            mLastKnownLocation = locationResult.getLastLocation();
                                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));

                                            currentLocationMarker = mMap.addMarker(new MarkerOptions()
                                                    .position(new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()))
                                                    .title("You are here")
                                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.oval)));
                                            mFusedLocationProviderClient.removeLocationUpdates(locationCallback);
                                        }
                                    };

                                    mFusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null);
                                }
                            }
                        } else {
                            Toast.makeText(MapsActivity.this, "Unable to get last location!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void showLocationPermission() {
        int permissionCheck = ContextCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    private void fetchDataforStations() {// pulling stations from api
        RequestQueue queue = Volley.newRequestQueue(MapsActivity.this);
        StringRequest getRequest = new StringRequest(Request.Method.GET, api_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray array = new JSONArray(response); // extraction of the array called articles
                            for (int i = 0; i <= array.length(); i++) { // pull all the array news until last of the data
                                JSONObject arrayJSONObject = array.getJSONObject(i);
                                String coordinates = arrayJSONObject.getString("center_coordinates");
                                String count = arrayJSONObject.getString("count");
                                String id = arrayJSONObject.getString("id");
                                String name = arrayJSONObject.getString("name");


                                String[] parts = coordinates.split(",");
                                String v = parts[0]; // before "," get
                                String v1 = parts[1]; // after "," get

                                LatLng position = new LatLng(Float.parseFloat(v), Float.parseFloat(v1));

                               Marker marker = mMap.addMarker(new MarkerOptions()
                                        .position(position)
                                        .title(name)
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.group_12)));
                               hashMap.put(marker,id+","+count);

                            }
                        } catch(JSONException e)
                        { // if there is a catch that would be a json issue
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MapsActivity.this, error.getMessage(), Toast.LENGTH_SHORT).show(); // showing volley error message
                    }
                }
        );
        getRequest.setRetryPolicy(new DefaultRetryPolicy( // creating single request for preventing double request at the same time
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(getRequest);
    }
}